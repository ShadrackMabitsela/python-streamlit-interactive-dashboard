import pandas as pd
import plotly.express as px
import streamlit as st

# Setup the basic configuration of the streamlit web-app.

st.set_page_config(
    page_title="Sales Dashboard",
    page_icon=":bar_chart:",
    layout="wide"
)


# Cache the dataframe to improve the web-apps performance when filtering.
@st.cache
def get_data_from_excel_file() -> pd.DataFrame:
    df: pd.DataFrame = pd.read_excel(
        "supermarkt_sales.xlsx",
        engine="openpyxl",
        sheet_name="Sales",
        skiprows=3,
        usecols="B:R",
        nrows=1000,
    )
    # Add the 'hour' column to the dataframe.
    df["hour"] = pd.to_datetime(df["Time"], format="%H:%M:%S").dt.hour
    return df


def main() -> None:
    """The entry point method of the program.\n"""
    df = get_data_from_excel_file()

    # --------------- Web Pages Side-Bar Section ---------------
    st.sidebar.header("Please Filter Here:")
    city = st.sidebar.multiselect("Select a city or cities:", options=df["City"].unique(), default=df["City"].unique())
    customer_type = st.sidebar.multiselect("Select a customer type:", options=df["Customer_type"].unique(), default=df["Customer_type"].unique())
    gender = st.sidebar.multiselect("Select a gender:", options=df["Gender"].unique(), default=df["Gender"].unique())

    # Filter the dataframe.
    df_selection = df.query("City == @city & Customer_type == @customer_type & Gender == @gender")

    # Main Page KPIs (Sales Key Performance Indicators)
    st.title(":bar_chart: Sales Dashboard")
    st.markdown("##")  # Paragraph markdown to create some space.

    # --------------- Top KPIs Section ---------------
    total_sales: int = int(df_selection["Total"].sum())
    average_rating: float = round(df_selection["Rating"].mean(), 1)
    star_rating: str = ":star:" * int(round(average_rating, 0))
    average_sales_by_transaction: float = round(df_selection["Total"].mean(), 2)

    # Display the KPI figures next to each other in three separate columns.
    left_column, middle_column, right_column = st.columns(3)
    with left_column:
        st.subheader("Total Sales:")
        st.subheader(f"US ${total_sales:,}")  # ',' Separate by the thousands.
    with middle_column:
        st.subheader("Average Rating:")
        st.subheader(f"{int(average_rating)} {star_rating}")
    with right_column:
        st.subheader("Average Sales Per Transaction")
        st.subheader(f"US ${average_sales_by_transaction}")

    st.markdown("---")  # Markdown separator.

    # --------------- Bar Chart Section ---------------
    # Sales by product line.
    sales_by_product_line = (
        df_selection.groupby(by=["Product line"]).sum()[["Total"]].sort_values(by="Total")
    )
    fig_product_sales = px.bar(
        sales_by_product_line,
        x="Total",
        y=sales_by_product_line.index,
        orientation="h",
        title="<em>Sales by Product Line</em>",
        color_discrete_sequence=["#0083B8"] * len(sales_by_product_line),
        template="plotly_white"
    )

    fig_product_sales.update_layout(
        plot_bgcolor="rgba(0,0,0,0)",  # Transparent background colour.
        xaxis=(dict(showgrid=False))
    )

    # Sales by hours.
    sales_by_hours = (
        df_selection.groupby(by=["hour"]).sum()[["Total"]]
    )
    fig_hourly_sales = px.bar(
        sales_by_hours,
        x=sales_by_hours.index,
        y="Total",
        title="<em>Sales by Hour</em>",
        color_discrete_sequence=["#0083B8"] * len(sales_by_hours),
        template="plotly_white"
    )

    fig_hourly_sales.update_layout(
        plot_bgcolor="rgba(0,0,0,0)",  # Transparent background colour.
        xaxis=(dict(tickmode="linear")),
        yaxis=(dict(showgrid=False))
    )

    left_column, right_column = st.columns(2)
    left_column.plotly_chart(fig_hourly_sales, use_container_width=True)
    right_column.plotly_chart(fig_product_sales, use_container_width=True)

    # --------------- Hide some of streamlit's default style elements  ---------------
    hide_st_style = """
        <style>
            #MainMenu {Visibility: hidden;}
            header {Visibility: hidden;}
            footer {Visibility: hidden;}
        </style>
    """
    st.markdown(hide_st_style, unsafe_allow_html=True)
    return None


# Check if the name of the file being executed ir 'main'.
if __name__ == "__main__":
    main()
